import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  disableYellowBox,
} from 'react-native';

import { StackNavigator, DrawerNavigator } from 'react-navigation'


//console.disableYellowBox = true;
console.ignoredYellowBox = ['Each'];
console.disableYellowBox = ['Warning: comp'];
console.warn('YellowBox is disabled.')

import { Screen1 } from './screens/Screen1'
import { Screen2 } from './screens/Screen2'
import { Screen3 } from './screens/Screen3'
import { Screen4 } from './screens/Screen4'
import { Screen5 } from './screens/Screen5'
import { LoginScreen } from './screens/LoginScreen'
import { SignupScreen } from './screens/SignupScreen'
import { ForgottenPasswordScreen } from './screens/ForgottenPasswordScreen'

const LoginStack = StackNavigator({
  LoginScreen: { screen: LoginScreen },
  SignupScreeen: { screen: SignupScreen },
  ForgottenPasswordScreen: { screen: ForgottenPasswordScreen } 
})

const DrawerStack = DrawerNavigator({
  Screen1: { screen: Screen1 },
  Screen2: { screen: Screen2 },
  Screen3: { screen: Screen3 },
  Screen4: { 
      screen: Screen4,
      navigationOptions: {
        title: '4. JSON'
      } 
    },
  Screen5: { screen: Screen5,
    navigationOptions: {
      title: '5. Dervis i Smrt'
    } 
  }
},{
  initialRouteName: 'Screen4'
})

const DrawerWrapper = StackNavigator({
  Fioka: { screen: DrawerStack,
    navigationOptions: ({navigation}) => {
      return {
          headerLeft: <Text onPress={() => navigation.navigate('DrawerOpen')} 
                            style={{ color: 'white', height: 50, width: 60, backgroundColor: 'green', fontWeight: 'bold', padding: 5 }}
                      >
                      MENU
                      </Text>,
              }
                                          },
          }
  })


const RootStack = StackNavigator({
  LoginStack: { screen: LoginStack },
  DrawerWrapper: { screen: DrawerWrapper }
},
  {
    headerMode: 'none',
    title: 'Main',
    initialRouteName: 'LoginStack',
    //initialRouteName: 'DrawerWrapper',
  }
)

export default RootStack;