import React, { Component } from 'react'
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image
  } from 'react-native';

import styles from '../assets/css'


export class LoginScreen extends Component {
    render(){
        return(
            <View style={styles.mainContainer}>
                    <View style={styles.buttonCss}>
                        <TouchableOpacity
                        onPress={()=> {this.props.navigation.navigate('DrawerWrapper')}}
                        >
                            <Text style={styles.buttonTxtCss}>LoginScreen</Text>
                        </TouchableOpacity>
                    </View>
            </View>
        )
    }
}