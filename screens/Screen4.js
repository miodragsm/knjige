import React, { Component } from 'react'
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    FlatList,
    Button,
    Alert,
  } from 'react-native';

import styles from '../assets/css'


var tekst = require('../package.json')

export class Screen4 extends Component {
    
    ispisi = () => {
        Alert.alert("JSON:", JSON.stringify(tekst));
    }

    render(){
        console.log('package.json tekst je --->'+JSON.stringify(tekst))

        return(
            <View style={styles.mainContainer}>

                <Text>
                    {JSON.stringify(tekst)}
                </Text>
                    <Button
                    title = "Go"
                    onPress={this.ispisi}
                    />
                    
            </View>
        )
    }
}