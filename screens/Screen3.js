import React, { Component } from 'react'
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image
  } from 'react-native';

import styles from '../assets/css'


export class Screen3 extends Component {
    render(){
        return(
            <View style={styles.mainContainer}>
                    <View style={styles.buttonCss}>
                        <TouchableOpacity
                        //onPress={()=> {this.props.navigation.navigate('DrawerToggle')}}
                        >
                            <Text style={styles.buttonTxtCss}>Screen 3, TEST drawer toggle</Text>
                        </TouchableOpacity>
                    </View>
            </View>
        )
    }
}