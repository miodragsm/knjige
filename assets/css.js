import { StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get("window")

const SCREEN_WIDTH = width
const SCREEN_HEIGHT = height

export default StyleSheet.create({
  boxCss:{
    justifyContent: 'center',
    alignItems: 'center',
    width: SCREEN_WIDTH /2 -10,
    height: SCREEN_HEIGHT/3 -40,
    backgroundColor: 'yellow',
    margin:5,
    borderRadius: 10,
  },
  buttonTxtCss:{
    width: SCREEN_WIDTH - 30,
    height: SCREEN_HEIGHT/3 -60,
    backgroundColor: 'gray',
    textAlign: 'center',
    textAlignVertical: 'center',
    fontSize: 20,
    fontWeight: 'bold',
  },
  headerContainer: {
    //width: SCREEN_WIDTH,
    padding: 5,
    //margin:5,
    justifyContent: 'center',
    alignItems: 'flex-start',
    backgroundColor: '#f4511e',
  },
  icon: {
    width: 24,
    height: 24,
  },
  buttonContainerCss:{
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'green',
  },
  mainContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: '#F5FCFF',
    padding: 10
  },
  navHeaderCss:{

  },
  regularTxtCss:{
    //textAlign: 'center',
    textAlignVertical: 'center',
    fontSize: 20,
  },
  titleTxtCss:{
    //backgroundColor: 'green',
    textAlign: 'center',
    textAlignVertical: 'center',
    fontSize: 20,
    fontWeight: 'bold',
  },

});